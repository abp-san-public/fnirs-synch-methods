#%% import
import pandas as pd
import os
from physynch import compute_distance
from config import BASEPATH, get_distance, normalize_signal

import random
random.seed(1234)

#%% set params
CLUSTER_DIR = 'clusters_norm' #
DISTANCE= 'cc' #run on all sinchrony measures: cc, wc, dtw

NORMALIZE = False
SYNCH_TYPE = 'synch'

#%%
for LAG_SECONDS in [0,1,2,5]:
    OUTDIR = os.path.join(BASEPATH, SYNCH_TYPE, CLUSTER_DIR, f'{DISTANCE}_{LAG_SECONDS}')
    DATADIR = os.path.join(BASEPATH, CLUSTER_DIR, 'TOG')
    
    os.makedirs(OUTDIR, exist_ok=True)
    
    distance = get_distance(DISTANCE, LAG_SECONDS)
    
    #for all sounds
    for SOUND in ['FEM_CRY', 'FEM_LAUGH', 'INF_CRY_HI', 'INF_CRY_LO', 'INF_LAUGH']:
        
        dyads = os.listdir(os.path.join(DATADIR, f'{SOUND}'))
    
        data_all = []
        indx = 0
    
        #for all clusters
        for CLUSTER in ['FrontalLeft', 'FrontalRight', 'MedialLeft', 'MedialRight']:
            
            #for all dyads
            for DYAD in dyads:
            
                #for all repetitions
                for REP in [1,2,3]: 
                
                    try: #not all dyads have all repetitions/sounds (?)
                        signal_m = pd.read_csv(os.path.join(DATADIR, SOUND, DYAD, 'M', CLUSTER, f'{REP}.csv'), index_col=0).values.ravel()
                        signal_f = pd.read_csv(os.path.join(DATADIR, SOUND, DYAD, 'F', CLUSTER, f'{REP}.csv'), index_col=0).values.ravel()
                        
                        if NORMALIZE:
                            signal_m = normalize_signal(signal_m)
                            signal_f = normalize_signal(signal_f)
                        
                        #decide whether M or F will load the STATIC
                        if random.getrandbits(1): #if 1: M loads the STATIC
                            signal_m_surr = pd.read_csv(os.path.join(DATADIR, 'STATIC', DYAD, 'M', CLUSTER, f'{REP}.csv'), index_col=0).values.ravel()
                            if NORMALIZE:
                                signal_m_surr = normalize_signal(signal_m_surr)
                            signal_f_surr = signal_f.copy()
                            
                        else:
                            signal_m_surr = signal_m.copy()
                            signal_f_surr = pd.read_csv(os.path.join(DATADIR, 'STATIC', DYAD, 'F', CLUSTER, f'{REP}.csv'), index_col=0).values.ravel()
                            if NORMALIZE:
                                signal_f_surr = normalize_signal(signal_f_surr)
                                
                        signals_available = True
                        
                    except:
                        print(f'{DYAD} - no repetition {REP}')
                        
                    if signals_available:
                        
                        dist = compute_distance(signal_m, signal_f, distance, detrend=False)
                        data_all.append(pd.DataFrame({'sound': SOUND, 
                                                      'dyad': DYAD, 
                                                      'cluster': CLUSTER,
                                                      'repetition': REP, 
                                                      'type': 'same', 
                                                      'distance': dist}, index=[indx]))
    
                        indx+=1
                        
                        dist_surr = compute_distance(signal_m_surr, signal_f_surr, distance, detrend=False)
                        data_all.append(pd.DataFrame({'sound': SOUND, 
                                                      'dyad': DYAD,
                                                      'cluster': CLUSTER,
                                                      'repetition': REP, 
                                                      'type': 'diff', 
                                                      'distance': dist_surr}, index=[indx]))
                        indx+=1
                
                #end for all repetitions
                
            #end for all dyads
            
        #end for all clusters
        data_all = pd.concat(data_all, axis = 0)
        data_all.to_csv(os.path.join(OUTDIR, f'{SOUND}.csv'))
    #end for all sounds